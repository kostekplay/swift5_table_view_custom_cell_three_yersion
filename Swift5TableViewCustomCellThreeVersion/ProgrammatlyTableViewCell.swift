////  ProgrammatlyTableViewCell.swift
//  Swift5TableViewCustomCellThreeVersion
//
//  Created on 03/11/2020.
//  
//

import UIKit

class ProgrammatlyTableViewCell: UITableViewCell {
    
    static let identifier = "ProgrammatlyTableViewCell"
    
    let myLabel: UILabel = {
        let l = UILabel()
        return l
    }()
    
    let myImageView: UIImageView = {
        let i = UIImageView()
        return i
    }()
    
    func configure(){
        
        contentView.addSubview(myLabel)
        contentView.addSubview(myImageView)
        
        myLabel.text = "Works !"
        myLabel.textAlignment = .center
        
        myImageView.image = UIImage(named: "image1")
        myImageView.contentMode = .scaleAspectFill
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        myImageView.frame = CGRect(x: 5, y: 5, width: 150, height: 75)
        myLabel.frame = CGRect(x: 160, y: 5, width: 100, height: 75)
        
        myImageView.layer.masksToBounds = true
        myImageView.layer.cornerRadius = 12
    }
    
}
