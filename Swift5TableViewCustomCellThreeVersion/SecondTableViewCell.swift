////  SecondTableViewCell.swift
//  Swift5TableViewCustomCellThreeVersion
//
//  Created on 03/11/2020.
//  
//

import UIKit

class SecondTableViewCell: UITableViewCell {
        
    @IBOutlet weak var myImageView: UIImageView!
    
    static let identifier = "SecondTableViewCell"
    
    static func nib() -> UINib {
        return UINib(nibName: "SecondTableViewCell", bundle: nil)
    }
    
    func configure(with imageName: String) {
        myImageView.image = UIImage(named: imageName)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
