////  ViewController.swift
//  Swift5TableViewCustomCellThreeVersion
//
//  Created on 03/11/2020.
//  
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let tableView: UITableView = {
        let t = UITableView()
        t.register(FirstTableViewCell.self, forCellReuseIdentifier: FirstTableViewCell.identifier)
        t.register(SecondTableViewCell.nib(), forCellReuseIdentifier: SecondTableViewCell.identifier)
        t.register(ProgrammatlyTableViewCell.self, forCellReuseIdentifier: ProgrammatlyTableViewCell.identifier)
        return t
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(tableView)
        tableView.frame = view.bounds
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 15
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row < 5 {
            let cell = tableView.dequeueReusableCell(withIdentifier: ProgrammatlyTableViewCell.identifier, for: indexPath) as! ProgrammatlyTableViewCell
            cell.configure()
            return cell
        }
        
        
        
        // second approuch
        if indexPath.row < 10 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: SecondTableViewCell.identifier, for: indexPath) as! SecondTableViewCell
            cell.configure(with: "image1")
            return cell
            
        }
        
        // first approuch
        let cell = tableView.dequeueReusableCell(withIdentifier: FirstTableViewCell.identifier, for: indexPath)
        cell.textLabel?.text = "OK !!!"
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row < 5 {
            return 150
        } else {
            return 50
        }
    }
    
}

